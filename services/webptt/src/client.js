// peer connection
var pc = null;

// data channel
var dc = null, dcInterval = null;

function error(e) {
    console.error(e);
    //alert(e);
}

function mute() {
    pc.getSenders().forEach((sender) => {
        if (sender.track.kind == 'audio') {
            sender.track.enabled = false;
        }
    });
    document.getElementById('ptt').classList.remove('active');
}

function unmute() {
    pc.getSenders().forEach((sender) => {
        if (sender.track.kind == 'audio') {
            sender.track.enabled = true;
        }
    });
    document.getElementById('ptt').classList.add('active');
}

function createPeerConnection() {
    var config = {
        sdpSemantics: 'unified-plan'
    };

    if (document.getElementById('use-stun').checked) {
        config.iceServers = [{ urls: ['stun:stun.l.google.com:19302'] }];
    }

    pc = new RTCPeerConnection(config);

    // connect audio / video
    pc.addEventListener('track', (evt) => {
        if (evt.track.kind == 'audio')
            document.getElementById('audio').srcObject = evt.streams[0];
    });

    return pc;
}

function populateSelect($select, devices) {
    let counter = 1;
    devices.forEach((device) => {
        const option = document.createElement('option');
        option.value = device.deviceId;
        option.text = device.label || ('Device #' + counter);
        $select.appendChild(option);
        counter += 1;
    });
};

function pupulateInputDevicesDropdown() {
    navigator.mediaDevices.enumerateDevices().then((devices) => {
        populateSelect(
            document.getElementById('audio-input'),
            devices.filter((device) => device.kind == 'audioinput')
        );
    }).catch((e) => {
        error(e);
    });
}

async function negotiate() {
    const localOffer = await pc.createOffer();
    await pc.setLocalDescription(localOffer);

    const gatheringComplete = new Promise((resolve) => {
        if (pc.iceGatheringState === 'complete') {
            resolve();
        } else {
            function checkState() {
                if (pc.iceGatheringState === 'complete') {
                    pc.removeEventListener('icegatheringstatechange', checkState);
                    resolve();
                }
            }
            pc.addEventListener('icegatheringstatechange', checkState);
        }
    });
    await gatheringComplete;

    // TODO: same as above?
    const offer = pc.localDescription;

    const resp = await fetch('offer', {
        body: JSON.stringify({
            sdp: offer.sdp,
            type: offer.type,
        }),
        headers: {
            'Content-Type': 'application/json'
        },
        method: 'POST'
    });
    const answer = await resp.json();

    document.getElementById('ptt').disabled = false;


    pc.setRemoteDescription(answer);
}

function connect() {
    document.getElementById('start').style.display = 'none';

    pc = createPeerConnection();

    // Build media constraints.

    const constraints = {
        audio: false,
        video: false
    };

    const audioConstraints = {};
    const device = document.getElementById('audio-input').value;
    if (device) {
        audioConstraints.deviceId = { exact: device };
    }
    constraints.audio = Object.keys(audioConstraints).length ? audioConstraints : true;

    // Acquire media and start negociation.

    if (constraints.audio || constraints.video) {
        
        navigator.mediaDevices.getUserMedia(constraints).then((stream) => {

            stream.getTracks().forEach((track) => {
                pc.addTrack(track, stream);
            });

            mute();

            return negotiate();

        }, (err) => {
            error('Could not acquire media: ' + err);
        });
    } else {
        negotiate().then(console.log).catch(error);
    }

    document.getElementById('stop').style.display = 'inline-block';
}

function disconnect() {
    document.getElementById('stop').style.display = 'none';
    document.getElementById('ptt').disabled = true;

    // close data channel
    if (dc) {
        dc.close();
    }

    // close transceivers
    if (pc.getTransceivers) {
        pc.getTransceivers().forEach((transceiver) => {
            if (transceiver.stop) {
                transceiver.stop();
            }
        });
    }

    // close local audio / video
    pc.getSenders().forEach((sender) => {
        sender.track.stop();
    });

    // close peer connection
    setTimeout(() => {
        pc.close();
    }, 500);
}

