import argparse
import asyncio
import os
import traceback
from typing import Dict, Optional, Set

import pyaudio
from aiohttp import web
from aiortc import MediaStreamTrack, RTCPeerConnection, RTCSessionDescription
from aiortc.mediastreams import MediaStreamError
from av.audio.frame import AudioFrame

print("#############################################################")
print()
print("  Initializing PyAudio...")
print("    This will print out a bunch of warnings - THIS IS NORMAL!")
print("    You can safely ignore them.")
print()

pya = pyaudio.PyAudio()

print()
print("#############################################################")
print()


##############
#   PLAYER   #
##############

class MediaStreamPlayer:
    """Plays an audio MediaStreamTrack using pyaudio."""

    track: MediaStreamTrack

    stream: Optional[pyaudio.Stream] = None
    task: Optional[asyncio.Task] = None

    def __init__(self, track) -> None:
        self.track = track
    
    async def start(self):
        """Start playing."""
        self.task = asyncio.ensure_future(self.run())

    async def stop(self):
        """Stop playing. Object should be discarded after this."""
        if self.task is not None:
            self.task.cancel()
            self.task = None

        if self.stream is not None:
            self.stream.stop_stream()
            self.stream.close()
            self.stream = None
    
    async def run(self):
        """Async loop to read and play frames from the track."""
        while True:
            # Read frame from webrtc
            try:
                frame: AudioFrame = await self.track.recv()
            except MediaStreamError:
                return
            
            # We have stream details now, so we can set up the pyaudio stream
            if self.stream is None:
                try:
                    self.stream = pya.open(
                        format=pyaudio.paInt16, # TODO: frame.format.name,
                        channels=len(frame.layout.channels),
                        rate=frame.sample_rate,
                        output=True,
                        
                    )
                except Exception as e:
                    traceback.print_exc()
                    return

            # Write the frame to the output
            self.stream.write(frame.planes[0].to_bytes())


#######################
#   WebRTC handling   #
#######################

peer_connections: Set[RTCPeerConnection] = set()
players: Dict[MediaStreamTrack, MediaStreamPlayer] = {}

async def offer(request: web.Request):
    """Receive SDP offer."""

    params = await request.json()
    offer = RTCSessionDescription(sdp=params["sdp"], type=params["type"])

    peer = RTCPeerConnection()
    peer_connections.add(peer)

    @peer.on("connectionstatechange")
    async def on_connectionstatechange():
        """Called when the connection state changes."""
        if peer.connectionState == "failed":
            await peer.close()
            peer_connections.discard(peer)

    @peer.on("track")
    async def on_track(track):
        """Called when the peer adds a new track."""
        if track.kind == "audio":
            player = MediaStreamPlayer(track)
            await player.start()

            players[track] = player

            @track.on("ended")
            async def on_ended():
                await player.stop()
    
    # Initialize the connection on our end
    await peer.setRemoteDescription(offer)    

    # Reply with an answer
    answer = await peer.createAnswer()
    await peer.setLocalDescription(answer)

    return web.json_response({
        "sdp": peer.localDescription.sdp,
        "type": peer.localDescription.type
    })


async def on_shutdown(app):
    # Close peer connections
    coros = [pc.close() for pc in peer_connections]
    await asyncio.gather(*coros)
    peer_connections.clear()


##########################
#   SERVE STATIC FILES   #
##########################

ROOT = os.path.dirname(__file__)

async def index(request):
    return web.FileResponse(os.path.join(ROOT, "index.html"))

async def javascript(request):
    return web.FileResponse(os.path.join(ROOT, "client.js"))


##################
#   ENTRYPOINT   #
##################

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="WebPTT server and player")
    parser.add_argument("--host", type=str, default="localhost", help="Host to listen on")
    parser.add_argument("--port", type=int, default=7000, help="Port to listen on")

    args = parser.parse_args()

    app = web.Application()
    app.on_shutdown.append(on_shutdown)

    app.router.add_get("/", index)
    app.router.add_get("/client.js", javascript)
    app.router.add_post("/offer", offer)

    web.run_app(
        app, 
        access_log=None, 
        host=args.host, 
        port=args.port,
    )
